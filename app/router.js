import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
    location: config.locationType
});

export
default Router.map(function() {
    this.route('login');
    this.resource('choices', function(){
        this.route('edit', { path: '/:choice_id' });
        this.route('new');
    });
    this.route('users');
});

