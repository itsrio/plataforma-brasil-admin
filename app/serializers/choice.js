import DS from 'ember-data';

export default DS.RESTSerializer.extend({
  attrs: {
    text_data: 'data'
  }
});
