import DS from 'ember-data';
import ENV from 'plataforma-brasil-admin/config/environment';

export default DS.RESTAdapter.extend({
  host: ENV.APP.api_url + '/admin'
});
