import Ember from 'ember';
import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin';
import ENV from 'plataforma-brasil-admin/config/environment';

export default Ember.Route.extend(AuthenticatedRouteMixin, {
//  model: function() {
//    return this.get('store').find('choice');
//  },
  setupController: function(controller) {
      var users_total = Ember.$.getJSON(ENV.APP.api_url + '/admin/users/count');

      users_total.then(function (data) {
        controller.set('users_total', data);
      });

      controller.set('model', this.get('store').find('user'));
  }
});
