import Ember from 'ember';
import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {
//  model: function() {
//    return this.get('store').find('choice');
//  },
  setupController: function(controller) {
    controller.set('model', this.get('store').find('choice'));
  }
});
