import Ember from 'ember';
import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin';

export
default Ember.Route.extend(AuthenticatedRouteMixin, {
    model: function() {
        // provide a new photo to the template
        return this.store.createRecord('choice');
    },
    deactivate: function() {
        this.currentModel.rollback();
    }
});
