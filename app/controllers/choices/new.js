import Ember from 'ember';

export
default Ember.Controller.extend({
    message: "",

    actions: {
        save: function() {
            var choice = this.get('model');
            choice.save();
            this.set('message', 'Operação efetuada com sucesso.');
            Ember.run.later(this, function() {
                this.set('message', '');
            }, 2000);
        }
    },
});
