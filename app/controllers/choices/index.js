import Ember from 'ember';

export
default Ember.ArrayController.extend({
    theFilter: "",

    checkFilterMatch: function(theObject, str) {
        var field, match;
        match = false;
        for (field in theObject) {
            if (theObject[field].toString().slice(0, str.length) === str) {
                match = true;
            }
        }
        return match;
    },

    filteredChoices: (function() {
        return this.get("arrangedContent").filter((function(_this) {
            return function(theObject, index, enumerable) {
                if (_this.get("theFilter")) {
                    return _this.checkFilterMatch(theObject, _this.get("theFilter"));
                } else {
                    return true;
                }
            };
        })(this));
    }).property("theFilter", "sortProperties", "model"),
    sortProperties: ['text_data', 'active', 'score', 'wins', 'losses'],
    sortAscending: false,
    actions: {
        sortBy: function(property) {
            this.set('sortProperties', [property]);
            this.toggleProperty('sortAscending');
            this.set('model', this.get('arrangedContent')); // set the model to the sorted array
        }
    }
});
