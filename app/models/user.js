import DS from 'ember-data';

export default DS.Model.extend({
      nome: DS.attr('string'),
      photo_url: DS.attr('string'),
      email: DS.attr('string'),
      cidade: DS.attr('string'),
      estado: DS.attr('string'),
      cpf: DS.attr('string'),
      facebook_id: DS.attr('string'),
      sector: DS.attr('string'),
      DeletedAt: DS.attr('string'),
      UpdatedAt: DS.attr('string'),
      CreatedAt: DS.attr('string', {
          defaultValue: function() { return new Date(); }
      })
  });
