//import { moment} from 'ember-moment';
import DS from 'ember-data';

export
default DS.Model.extend({
    active: DS.attr('boolean'),
    created_at: DS.attr('string'),
    creator_id: DS.attr('string'),
    text_data: DS.attr('string'),
    item_id: DS.attr('string'),
    local_identifier: DS.attr('string'),
    losses: DS.attr('string'),
    position: DS.attr('string'),
    prompt_id: DS.attr('string'),
    prompts_count: DS.attr('string'),
    prompts_on_the_left_count: DS.attr('string'),
    prompts_on_the_right_count: DS.attr('string'),
    question_id: DS.attr('string'),
    ratings: DS.attr('string'),
    request_id: DS.attr('string'),
    score: DS.attr('string'),
    tracking: DS.attr('string'),
    updated_at: DS.attr('string'),
    version: DS.attr('string'),
    wins: DS.attr('string'),
});
